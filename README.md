# Wallet

This is a password wallet, based on an article in Linux Journal #165 by
Carl Welch. It was originally strictly a command line tool. I've
modified it extensively to be interactive.

## Problems with existing programs

There are numerous problems with most existing password wallet programs.
Here are some.

1.  Many password wallet programs must be paid for or require commercial
    software. This introduces no end of complications. Why should you
    have to pay for the right to encrypt and decrypt your own passwords?
    What happens if the company goes out of business or is bought by
    some other company which isn't particularly friendly to users of the
    program? Is the company storing your passwords in some sort of cloud
    account?
2.  Most password managers let you store a password for a given site and
    no more. What if you want to store a username, a URL, some security
    questions and their answers (like banking sites)?
3.  Password programs may or may not use an encryption method you are
    familiar with, or they may not even tell you what method they use.
    If you want to *change* password managers, you'll be hard pressed to
    work out how.

## Requirements

The following are programs which must be installed for **Wallet** to
run:

-   gpg
-   dialog
-   mktemp
-   less

The X-Window version (`xwallet`) requires `zenity` instead of `dialog`.

**Wallet** will test for the presence of these programs when it runs,
and abort with an error message if it finds one of them missing.

## Installation

Copy the `wallet` program to some executable directory. Copy the
`.walletrc` to your home directory.

## Operation

**Wallet** encrypts an entire file of whatever you like. Ideally, you
will organize your entries logically, but it's up to you. **Wallet**
uses GPG to encrypt and decrypt the file. It is a `bash` script meant to
run under Linux.

When you start the program, you are presented with a menu of three
options:

1.  View File: This asks for your master password and then decrypts the
    file, feeding it to `less` to allow you to view the file. If the
    file doesn't exist, it puts you in edit mode to create it. (The
    X-Window version uses zenity to display the file.)
2.  Edit File: This asks for your master password, decrypts your file,
    and opens the editor, allowing you to edit the file.
3.  Different File: This asks for your master password, asks for the
    name of the file to open, and attempts to open it for viewing. If
    the file doesn't exist, it attempts to create it.

The **Wallet** program relies on a file called `~/.walletrc`, which
contains two values. The first is

    WALLET_FILENAME=~/.wallet.gpg

This is your password file. You can rename it as you like. The second
value is:

    VISUAL=vim

This is your editor. You can change it to `nano`, or some other editor
you prefer.

`.walletrc` is included with this distribution.

If you need to create a "clear text" version of your password file, you
need to launch **wallet** and opt to edit the file. Once in your editor,
you must use whatever editor option allows you to store your data in a
different file, which it will do **unencrypted**.

## Importing an existing password file

When you first start up **wallet**, you will have no encrypted password
file, and the program will put you into edit mode. Assuming you have a
cleartext password file somewhere else, you can read this file into your
editor, and then save the file and exit your editor. **wallet** will
encrypt the file and store it for you.

## Web version

I've also written a version of **wallet** for the web, suitable for use on
your local machine (with a local web server). It is called **wallet.php**.
Simply install it anywhere your web server can see it. All it can do is
*view* your password file. It operates in a way similar to the bash
version.

## Security

I am not a security expert or a hacker, nor have I consulted one in
building this application. It is possible that, *while `wallet` is
running*, it could expose your password file to others. Thus, I
recommend you run this application on your local machine, and close it
when it's not in use. When **wallet** is *not* in use, your password
file is suitably encrypted.

## License

This software is provided "as is". I make no claims for its suitability
to any purpose. Your use of it is governed under the GNU Public License,
version 2.0. This means you can use it or alter it in any way you like.
And you may distribute it altered or unaltered as you like, so long as
you include the source code (which is the script itself in this case).

**Wallet** can be obtained on
[Gitlab](http://gitlab.com/paulmfoster/wallet).

